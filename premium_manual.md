# <center> Data Services Dashboard </center>
### <center> User Manual </center>


<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](./images/axain_main.jpg)
</div>
<hr/>

##Preface
This user manual describes the different elements and features of the BI dashboard, ensuring that you know what is what. 

The manual consists of five sections. The first four sections describe the different sheets the dashboard comprises
and the individual components of each distinct sheet. The last section of this user guide is a functionality guide
describing the different tools at your disposal to maneuver through QuickSight,
the dashboarding tool used for this dashboard.

<hr/>

## Table of Contents
1. [Sheets](#Sheets)
   
    1.1 [Management](#Management)
   
    1.2 [Totals](#Totals)

    1.3 [Usage Analysis](#Usage analysis)
   
    1.4 [Battery Insights](#Battery insights)  
   

2. [QuickSight Functionalities](#Functionality guide)

<hr/>

## 1. Sheets
For each of the four distinct sheets contained in the dashboard we shall provide you with detailed information
on the individual components incorporated in the sheet.
### 1.1 Management <a name="Management"></a>
This sheet comprises a selected number of KPIs in a concise overview. For each of the KPIs trends can be analyzed and
distinct periods can be compared. The following six KPIs can be found on this page:
1. Claimed bikes - Bikes that have been claimed by a user, in possession of the user or not
2. Non-activated bikes - Bikes that have been claimed by a user, but not yet in possession of the user
3. Connected bikes - Bikes that are in possession of the user
4. Days to activation - The number of days between a bike being claimed and connected
5. Loss of subscription - Users that no longer have a subscription
6. Users

Below an example is shown of the KPI **Claimed bikes**. On the left-hand side the trend of the KPI is displayed with 
months on the x-axis and each year in a distinct color. The x-axis can be modified using the control panel found on the
top of the page. A choice can be made between months, quarters or years.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![GIB](images/mgmt_kpi_claimed.png)
</div>

On the right-hand side of the visual values of the current and last month are shown, together with a comparison between
the two. Instead of the current and last month, the current and last quarter or current and last year can be compared 
using the control introduced above. By default, the comparison is on a 'to date' basis. This means that on April 12th, 
the KPI is compared for the first 12 days of April and March. If desired, the second control in the panel can be used 
to compare the current month with the complete prior month. This is achieved by selecting 'period' instead of 'to date'.

### 1.2 Totals <a name="Totals"></a>
The KPIs on this page display the current status of various metrics concerning the bike fleet 
and the user base. The KPIs are divided into three categories: bikes, users and dealers. Individual KPIs are grouped
by means of the icons found on the left-hand side indicating the respective groups. 

The first figures (also shown below) convey the KPIs of the bikes, users and dealers categories respectively.
The number is a static value indicating the current quantity of the specified metric. The blue trend line 
on the right-hand side of the KPI shows the gradual increase/decrease in the value of the KPI over time.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![GIB](images/General_Insights_Bikes.png)
</div>

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/General_insights_users.png)
</div>

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_gi3.png)
</div>

### 1.3 Usages Analysis<a name="Usage analysis"></a>
At the very top of the Usage Analysis sheet a control panel with several filters can be found.
Each of these filters applies to all the visualizations found on this sheet. By clicking on the horizontal bar the
range of possible filters unfold and can be applied.

Note that the bar charts on this sheet can be filtered by clicking on an individual bar. For more information
please refer to [use of filters/controls](#Use of filters/controls).

#### KPI's

The first visuals displayed on this page are three KPIs regarding bike rides, where the distinguishing feature is the
time period: day, month and total. 

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua1.PNG)
</div>

#### Bike ride general statistics

You will find 4 charts displaying general information regarding the bike rides in this section.

The first two figures at the top show the number of bike rides over time. Do note that these are not cumulative figures.
The first figure on the left contains a horizontal line indicating the average number of bike rides over the specified 
time period. The bar chart on the right-hand side provides the ability to drill down to different time units, varying 
between months, quarters or years. In the image below you can see the number of bike rides for the months of June and 
July in 2021.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua3.PNG)
</div>

The bar chart on the bottom-left of this section displays the number of bike rides per day of the week, while
the final figure on the bottom-right shows the average distance (in km) of the bike rides. The average number of bike
rides is calculated over the entire dataset, unless a different time period has been specified in the filter panel. By
hovering over the bars or points on the line graph the exact values of the day of the week are displayed.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua4.PNG)
</div>

#### Weather conditions

The two graphs in this section provide insights regarding the number of bike rides based on specific weather conditions.

The first figure is a histogram that shows the number of bike rides against a temperature range. Each bin contains all
bike rides where the temperature in the middle of the ride (geographically speaking) is within a span of 3 degrees
Celsius. For example, a bike ride with a temperature of 5 degrees Celsius is categorized in the bin of 3-6 degrees.

The second figure is a bar chart that shows the average number of bike rides based on a specific weather condition.

Similar to previous figures, hovering over the bars provides exact information on the values.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua5.PNG)
</div>

#### Speed & Duration Insights

The bar chart in this section displays the average speed measured during the trips of specific bikes, categorized by 
type. The speed is displayed in kilometers per hour. The line graph to the right of the bar graph shows the average 
duration of bike rides throughout time, with a distinction between weekdays and weekend days to potentially taxonomize
specific types of bike users.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua6.PNG)
</div>

#### Insights per type

The first bar chart shows the average number of bike rides per day based on the type of bike.
The second bar chart on the right-hand side shows the number of users per tag. A tag is a category based on certain
thresholds. This chart makes use of the down-drilling option, which enables users to see different visualizations
in one chart. As For more information regarding down-drilling please refer to the section
[Downdrilling on the graphics](#Downdrilling on the graphics).

The visual on the right-hand side consists of four nested visualizations representing varying custom created tags.
The first tag divides users into two groups: 
frequent users and infrequent users. A frequent user is a user making bike rides at least once every two days.
An infrequent user is a user not fulfilling this requirement. 
The second tag visualized divides users into three groups: Weekday non-rush hour users, weekend users and 
weekday rush hour users. The third tag puts users into three groups: small distance travellers, medium distance travellers
and long distance travellers. Small distance travellers are travellers where at least 50 percent of the trips
cover less than 5 kilometers. Medium distance travellers are users that have at least 50 percent of their trips in the
range between 5 and 9 kilometers. The long distance travellers are users where at least 50 percent of their trips
cover more than 9 kilometers. The last tag concerns the maturity of the users, describing when users initialized 
their subscription. There are two categories in this group: Very mature users and immature users. An immature user is 
a user that is less than 60 days active on the platform. A very mature user is a user that has been active for more than
1 year.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua7.PNG)
</div>

#### Geographical information
The last bart chart also allows for drilling down to different levels. This bar chart shows three different levels:
1. The amount of bike rides on country level; 
   
2. The amount of bike rides on region level;
   
3. The amount of bike rides on municipality level.

The last chart, a map visualization, shows the aggregated number of bike rides based on latitude and longitude
coordinates. The bike rides are masked so that the visual shows locations that could be off by several
margins to ensure that privacy concerns are dealt with.

### 1.4 Battery insights <a name="Battery insights"></a>

This final sheet provides insights on the usage and capacity of the batteries in the e-bikes. At the top of the screen
several tiles with general statistics are shown:

* SoC - Average state of charge at the start of a trip
* SoC Consumption - Average state of charge consumption per trip
* Driving range - Average estimated maximum battery driving range
* Support mode - Average selected support mode (0-5) per trip
* BFCC  - Average battery full charge capacity in ampere hour

The first two statistics show information regarding battery percentages and consumption. SoC is an abbreviation
for State of Charge, an indication of the current battery % remaining. The consumption indicates the % of
battery consumed per kilometer. 

The third statistic indicates the driving range of the battery. This equates to the distance traveled divided by
the SoC consumption, multiplied by 100. In other words, it represents the potential distance that can be traversed
with the bike if the battery is at full charge. This statistic is a maximum, as it is an optimistic estimation
of the driving range.

The support mode is the fourth statistic. It displays the average support mode utilized by the users. A 
support mode can be understood as a level of power support provided by the electrical motor, supporting the
cyclist. It ranges from 0 (no support) up to 5 (maximum support).

Finally, the battery full charge capacity (BFCC) is provided. Similar to the driving range, it gives an estimation of
the potential of the battery if fully charged. However, it differs from the driving range by considering the
electrical capacity instead of the distance capacity.

#### General statistics

Down below, the statistics several charts can be found. Each of these bar charts can be used as a filter by selecting
an individual bar corresponding to a specific categorical value. 

The first bar chart displays the number of rides
made per dominant support mode per day. The dominant support mode represents the support mode used most often
proportionally during a ride. For example, if a user cycles 30% in support mode 0, 40% in support mode 1, 25% 
in support mode 2 and 5% in support mode 4, then the dominant support mode of this specific ride equals 0. The number
of rides are displayed on the x-axis and the support modes on the y-axis.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/support_mode.PNG)
</div>

The second bar chart shows the average estimated battery driving range, where the driving range is expressed 
in kilometers on the x-axis and the different support modes on the y-axis. 

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/driving_range.PNG)
</div>

Thirdly, the battery range is displayed in a binned fashion (0-10, 10-20, 20-30, etc.) on the x-axis and the number
of bikes corresponding to each of the created bins found on the y-axis. The driving range, as explained before, indicates
the potential distance that could be traveled if the battery were fully charged.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/battery_range.PNG)
</div>

Finally, the rate at which the battery % decreases per kilometer on the x-axis is displayed per support mode
on the y-axis. This is calculated by considering the battery % at the start of a ride and at the end of the ride, and
subsequently dividing the distance traveled by the difference between these two values.

#### Support mode insights
Instead of focusing on the dominant support mode, the visualizations below display the relation between the 
distribution of support modes used and several other metrics. The first figure displays the distribution of support modes 
used per type of bike, while the second shows the variation in the distribution of support mode usage over time,
displayed on a monthly basis. The latter allows for retrieving insights in, e.g., seasonality effects.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/support_mode_insights_1.PNG)
![](images/support_mode_insights_2.PNG)
</div>

#### Estimated driving range per support mode
In the subsequent section, containing a variety of scatter plots, the relation between outside temperature and
estimated driving range is displayed for each distinct support mode. The estimated driving range on the x-axis ranges
from 0 to 100 kilometer, while the outside temperature on the y-axis ranges from 0 to 40 degrees Celcius.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/support_mode_temp.PNG)
</div>

#### Battery full charge capacity insights
The final section on this sheet provides information regarding the battery full charge capacity. As priorly 
stated, this is an indication of current state of quality of the battery.

The first two visualizations displayed below show the relation between bike usage and the battery full
charge capacity. The first visualization uses the age of the bikes (in months) as an indication of bike usage,
while the second visualization makes use of the charging cycles of the bike. A charging cycle can be interpreted
as a single charging event where the bike recharges its battery. 

The latter two visualizations attempt to display the relation between the battery capacity and other metrics. However,
they differ from the previous two visualizations by considering the number of charging cycles as a binned variable.
As such, it is here considered a categorical variable. The first bar chart reconsiders the dominant support mode
as a factor affecting the state of the battery quality. In a similar fashion, the second bar chart shows how
the battery full charge capacity might be influenced by different bike models.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/BFCC_depreciation.PNG)
![](images/BFCC_depreciation_2.PNG)
![](images/BFCC_depreciation_3.PNG)
![](images/BFCC_depreciation_4.PNG)
</div>

### 2. QuickSight Functionalities <a name="Functionality guide"></a>
This section describes the general functionality features of the dashboard. We describe how the filters can be applied,
how down-drilling on visuals can lead to more insights, and how the underlying data can be exported to csv.

#### Use of filters/controls
In order to obtain the insights for a specific selection (e.g., data, bike type, user types etc.) the dashboard contains 
different filters. By unfolding a specific filter in the control panel the user can select the preferred subset of the data
in order to get more detailed insights. In the sheet <i>General Insights</i> these filters are positioned next to the
visuals they applies to. However, in the sheets <i>Usage Analysis</i> and <i>Battery Insights</i> 
they are placed in the control panel and are subsequently applicable to all visuals on the sheet. In the image below
the filter panel of the <i>Usage Analysis</i> sheet can be seen. In order to make selections the control panel 
should be unfolded.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/controls.png)
</div>

Note that the controls/filters can be reset by clicking on the 3 dots and subsequently selecting <i>reset</i>.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

[](images/filterreset.png)
</div

Besides filtering using the controls, there is also the option to filter bar charts by clicking on a specific bar inside
the chart. Below a figure is shown where the number of bike rides are filtered on the weather type cloudy.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/Filter_by_click.png)
</div>

All figures in the screen are now filtered on cloudy bike rides. 
The filtered bar keeps the original color value whereas the colors of the other bars have become transparent.
In order to reset the filter one can simply re-click on the bar selected.

#### <a name = "downdrill"></a> Down-drilling on the graphics

In some of the visuals a different level of detail can be found by utilizing the down-drilling
option. For example, in the user tag visual on the <i>Usage Analysis</i> sheet one can drill down through
the different user tag types (frequency, moment of usages etc.) in order to see the number of users per category.
In the figure below, the arrows on the right side of the visual can be used for down-drilling and up-drilling.
Hence, if a visual displays these arrows, it means that a drilling hierarchy exists and can be utilized.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/downdrilling.png)
</div>

#### Exporting underlying data
Each visual has the option to export the data of the analysis to a csv file. On the right-hand side of the visual, 
click on the three vertical dots opening up a list of options. Subsequently, select <i>export to CSV</i> to export
the data to a csv file.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/exporting_data.png)
</div>

#### Save dashboard as pdf
In order to print or save the current dashboard page as a pdf file the user can select the <i>Print</i> option on the
upper-right corner of the dashboard. 

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/pdfprinting.png)
</div>

#### Make forecast
The visuals with a time-span on the x-axis can be enriched with a forecast. By using this method QuickSight is able to
make predictions for the y-values corresponding to the time period specified in the <i>Forecast proporties</i>.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/forecast.png)
</div>

# <center> User Manual Data Services Dashboard </center>
<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](./images/axain_main.jpg)
</div>
<hr/>

##Preface
This user manual describes the different elements and features of the BI dashboard, ensuring that you know what is what. 

The manual consists of four sections. The first three sections describe the different sheets the dashboard comprises
and the individual components of each distinct sheet. The last section of this user guide is a functionality guide
describing the different functionalities of QuickSight, the dashboarding tool used for this dashboard, at your disposal.

<hr/>

## Table of Contents
1. [Sheets](#Sheets)
   
    1.1 [General Insights](#General insights)
   
    1.2 [Usage Analysis](#Usage analysis)
   
    1.3 [Battery Insights](#Battery insights)  
   

2. [QuickSight Functionalities](#Functionality guide)

<hr/>

## 1. Sheets
For each of the three distinct sheets we shall provide you with detailed information on the individual components 
comprising the sheet.
### 1.1 General insights <a name="General insights"></a>
The first sheet of the dashboard can be split up into two categories of data: 
<strong>KPIs</strong> and <strong>Trends</strong>. KPIs are intended to provide you with insights
regarding the current status of your key figures. Trends, on the other hand, 
display the change in measured key figures over time.
#### KPIs
The KPIs, found at the top of the page, display the current status of various metrics concerning the bike fleet 
and the user base. The KPIs are divided into three categories: bikes, users and dealers. Individual KPIs are grouped
by means of the icons found on the left-hand side indicating the respective groups. 

The first figures (also shown below) convey the KPIs of the categories bikes, users and dealers respectively.
The number is a static number indicating the current number of the specified metric. The blue trend line 
on the right-hand side of the KPI shows the gradual increase/decrease in the value of the KPI over time.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![GIB](images/General_Insights_Bikes.png)
</div>

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/General_insights_users.png)
</div>

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_gi3.png)
</div


#### Trend figures

Subsequently, this sheet contains several trends delivering further insights to the user. 

Firstly, you can observe two figures that show monthly/quarterly/yearly trends of registered bikes and users.
The figures are displayed below:

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/General_insights_trend_figures.png)
</div>

The figure on the left shows the number of registered bikes over time, while the figure on the right displays
the number of users over time.
It is possible to drill down to a different date level by pressing the drill down/up button. It is by default
displaying the figures per month, but this can be changed to quarter or year with the click of a button. 
For more information about downdrilling please refer to section
[Downdrilling on the graphics](#a-name--downdrilla-downdrilling-on-the-graphics).

The final two figures simiarly show the number of registered bikes and the number of users, but this time in a 
cumulative fashion. With the aid of the filter panels a specific time period can be selected enabling the user
to examine growth in bikes or user base over a specific time interval. Furthermore, it is possible to filter the
registered bikes over time on a (or multiple) dealer(s).

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_gi5.PNG)
![](images/doc_gi6.PNG)
</div>

### 1.2 Usages Analysis<a name="Usage analysis"></a>
At the very top of the Usage Analysis sheet a control panel with several filters is shown.
Each of the filters applies to all the visualizations shown on this sheet. By clicking on the horizontal bar the
range of possible filters unfold and can be applied.

Note that the bar charts on this sheet can be filtered by clicking on an individual bar. For more information
please refer to [use of filters/controls](#Use of filters/controls).

#### KPI's

The first visuals displayed on this page are three KPIs regarding bike rides, where the distinguishing feature is the
time period: day, month and total. 

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua1.PNG)
</div>

#### Bike ride general statistics

You will find 4 charts displaying general information regarding the bike rides in this section.

The first two figures at the top show the number of bike rides over time. Do note that these are not cumulative figures.
The first figure on the left contains a horizontal line indicating the average number of bike rides over the specified 
time period. The bar chart on the right-hand side provides the ability to drill down to different time units, varying 
between months, quarters or years. In the image below you can see the number of bike rides for the months of June and 
July in 2021.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua3.PNG)
</div>

The bar chart on the bottom-left of this section displays the number of bike rides per day of the week, while
the final figure on the bottom-right shows the average distance (in km) of the bike rides. The average number of bike
rides is calculated over the entire dataset, unless a different time period has been specified in the filter panel. By
hovering over the bars or points on the line graph the exact numbers of the day of the week are displayed.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua4.PNG)
</div>

#### Weather conditions

The two graphs in this section provide insights regarding the number of bike rides based on specific weather conditions.

The first figure is a histogram that shows the number of bike rides against a temperature range. Each bin contains all
bike rides where the temperature in the middle of the ride (geographically speaking) is within a span of 3 degrees. For 
example, a bike ride with a temperature of 5 degrees is categorized in the bin of 3-6 degrees.

The second figure is a bar chart that shows the average number of bike rides based on a specific weather condition.

Similar to previous figures, hovering over the bars provides exact information on the numbers.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua5.PNG)
</div>

#### Speed & Duration Insights

The bar chart in this section displays the average speed measured during the trips of specific bikes, categorized by 
type. The speed is displayed in kilometers per hour. The line graph to the right of the bar graph shows the average 
duration of bike rides throughout time, with a distinction between weekdays and weekend days to potentially taxonomize
specific types of bike users.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua6.PNG)
</div>

#### Insights per type

The first bar chart shows the average number of bike rides per day based on the type of bike.
The second bar chart on the right-hand side shows the number of users per tag. A tag is a category based on certain
thresholds. This chart makes use of the down drilling option, which enables users to see different visualizations
in one chart. As For more information regarding downdrilling please refer to the section
[Downdrilling on the graphics](#Downdrilling on the graphics).

The visual consists of four visualizations representing varying custom created tags. The first tag divides
users in two groups: 
frequent users and infrequent users. A frequent user is a user that makes bike rides at least once every two days.
An infrequent user is a user that doesn't fulfill this requirement. 
The second tag that is visualized divides users in three groups: Weekday non-rush hour users, weekend users and 
weekday rush hour users. The third tag puts users in three groups: small distance travellers, medium distance travellers
and long distance travellers. Small distance travellers are travellers where at least 50 percent of the trips
are less than 5 kilometers. Medium distance travellers are users that have at least 50 percent of their trips in the
range between 5 and 9 kilometers. The long distance travellers are users where at least 50 percent of their trips
are longer than 9 kilometers. The last tag is about the maturity of the users, describing when users initialized 
their subscription. There are two categories in this group: Very mature user and immature user. An immature user is 
a user that is less than 60 days active on the platform. A very mature user is a user that has been active more than
1 year.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/doc_ua7.PNG)
</div>

#### Geographical information
The last bart chart also allows for drilling down to different levels. This bar chart shows three different levels,
the first on country level which shows the amount of bike rides in a country. Then next visualization shows the amount
of bike rides on region level. The last visualization shows the amount of bike rides on municipality level.

The last chart which is a map visualization shows aggregated bike rides based on latitude and longitude.
The bike rides are masked so that the visual shows locations that could be off some distance to ensure that
privacy concerns are dealt with.

### 2. QuickSight Functionalities <a name="Functionality guide"></a>
This section describes the general functionality features of the dashboard. We describe how the filters can be applied,
how downdrilling on visuals can lead to more insights, how the underlying data can be exported to csv.

#### Use of filters/controls
In order to obtain the insights for a specific selection (e.g. data, bike type, user types etc.) the dashboard contains 
different filters. By folding out a specific filter in the control panel the user can select the prefered subset of the data
in order to get more detailed insights. In the sheet general insights these filters placed next to those visuals it applies on, in
the sheets Usages analysis and Battery insights they are placed in the control panel and applicable to all visuals. Below the controls 
of the Usage analysis sheet can bee seen, in order to make selections the control panel 
should be folded out.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/controls.png)
</div>

Note that the controls/filters can be reset by clicking on the 3 dots and "reset".

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

[](images/filterreset.png)
</div

Besides filtering with controls, there is also the option to filter barcharts by clicking on a specific bar in a chart.
Below a figure is shown where the user filtered on the weather type cloudy.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/Filter_by_click.png)
</div>

All figures in the screen are now filtered on cloudy bike rides. 
The filtered bar keeps the original color value whereas the colors of the other bars have become transparent.
To reset the filter one can simply click on the bar selected, in this case the bar that represents cloudy bike rides.

#### <a name = "downdrill"></a> Downdrilling on the graphics

In some of the visuals a different level of detail can be selected by making use of the downdrilling
option. In for example the user tag visual on the user analysis sheet one can downdrill over
the different user tag types (frequency, moment of usages etc.) to see the number of users per. Below can be seen how
the arrows on the right side of the visual can be used for downdrilling and updrilling.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/downdrilling.png)
</div>

#### Exporting underlying data
Each visual has the option to export the data of the analysis to csv. On the right side of the visual 
the three dots and the "export to CSV" can subsequently be selected.

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/exporting_data.png)
</div>

#### Save dashboard as pdf
In order to print or save the current dashboard page as a pdf the user can select the "print" option on the
right top corner of the dashboard. 

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/pdfprinting.png)
</div>

#### Make forecast
The visuals with time one the x-axis can be enriched with a forecast. The dashboard can then
make predictions for the y-values for the days specified in the "Forecast proporties".

<div style="width:75%; display: block; margin-left: auto; margin-right: auto;">

![](images/forecast.png)
</div>